package com.ash.tabibytask.models;

import com.ash.tabibytask.utils.GeneralClass;

/**
 * Created by ahmed.shafeek on 2/18/2017.
 */

public class GovernModel {

    private String id;
    private String name_ar;
    private String name_en;
    private String create_time;
    private String update_time;
    private String country_id;
    private String country_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String toString(){
        if(GeneralClass.language.equals("en"))
            return getName_en();
        else
            return getName_ar();
    }
}
