package com.ash.tabibytask.models;

/**
 * Created by amr on 11/22/16.
 */
public class StandardWebServiceResponse {

    private String code;
    private String message;
    private Object governs;
    private Object specs;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getGoverns() {
        return governs;
    }

    public void setGoverns(Object governs) {
        this.governs = governs;
    }

    public Object getSpecs() {
        return specs;
    }

    public void setSpecs(Object specs) {
        this.specs = specs;
    }
}
