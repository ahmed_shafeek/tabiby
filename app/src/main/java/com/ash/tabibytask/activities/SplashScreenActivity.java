package com.ash.tabibytask.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.ash.tabibytask.R;
import com.ash.tabibytask.serverconnection.Url;
import com.ash.tabibytask.serverconnection.volley.AppController;
import com.ash.tabibytask.serverconnection.volley.ConnectionVolley;
import com.google.gson.Gson;

import java.util.Locale;

public class SplashScreenActivity extends Activity implements Response.Listener, Response.ErrorListener {

    Handler handler;
    String url, governResponse, specializationResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);
        initialization();
//        set();
        callServerToGetGovern();
        checkLanguage("ar");
    }

    public void checkLanguage (String languageToLoad){
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
    }

    private void callServerToGetGovern(){
        url = Url.getInstance().governURL;
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.GET, Url.getInstance().governURL, this, this, "", false);
        connectionVolley.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void callServerToGetSpecialization(){
        url = Url.getInstance().specializationURL;
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.GET, Url.getInstance().specializationURL, this, this, "", false);
        connectionVolley.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void initialization(){
        handler = new Handler();
    }

    private void set(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toHome();
            }
        }, 3000);
    }

    private void toHome(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("governResponse", governResponse);
        intent.putExtra("specializationResponse", specializationResponse);
        startActivity(intent);
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            if (url.equals(Url.getInstance().governURL)){
                governResponse = response.toString();
                callServerToGetSpecialization();
            } else if (url.equals(Url.getInstance().specializationURL)){
                specializationResponse = response.toString();
                toHome();
            }
        } catch (Exception e){
            Toast.makeText(this, getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        Toast.makeText(this, getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
