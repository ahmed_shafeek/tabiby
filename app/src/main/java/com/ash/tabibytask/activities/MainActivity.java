package com.ash.tabibytask.activities;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ash.tabibytask.R;
import com.ash.tabibytask.fragments.HomeFragment;

public class MainActivity extends FragmentActivity {

    String governResponse, specializationResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialization();
        openHomeFragment();
    }

    private void initialization(){
        governResponse = getIntent().getStringExtra("governResponse");
        specializationResponse = getIntent().getStringExtra("specializationResponse");
    }

    private void openHomeFragment(){
        HomeFragment homeFragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString("governResponse", governResponse);
        args.putString("specializationResponse", specializationResponse);
        homeFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(R.id.contentView, homeFragment).commit();
    }
}
