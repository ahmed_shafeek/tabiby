package com.ash.tabibytask.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.ash.tabibytask.R;
import com.ash.tabibytask.models.GovernModel;
import com.ash.tabibytask.models.SpecializationModel;
import com.ash.tabibytask.models.StandardWebServiceResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed.shafeek on 2/19/2017.
 */

public class HomeFragment extends Fragment implements View.OnClickListener{

    View view;
    String governResponse, specializationResponse;
    ArrayList<GovernModel> governModelArrayList;
    ArrayList<SpecializationModel> specializationModelArrayList;
    Spinner specializationSpinner, governSpinner, typeSpinner;
    ImageView typeArrowImageView, governArrowImageView, specializationArrowImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment, container, false);
        initialization();
        decodeGovernResponse();
        decodeSpecializationResponse();
        spinnerAdapter();
        set();
        return view;
    }

    private void initialization(){
        specializationSpinner = (Spinner) view.findViewById(R.id.specializationSpinner);
        governSpinner = (Spinner) view.findViewById(R.id.governSpinner);
        typeSpinner = (Spinner) view.findViewById(R.id.typeSpinner);
        typeArrowImageView = (ImageView) view.findViewById(R.id.typeArrowImageView);
        governArrowImageView = (ImageView) view.findViewById(R.id.governArrowImageView);
        specializationArrowImageView = (ImageView) view.findViewById(R.id.specializationArrowImageView);
        governResponse = getArguments().getString("governResponse");
        specializationResponse = getArguments().getString("specializationResponse");
        governModelArrayList = new ArrayList<>();
        specializationModelArrayList = new ArrayList<>();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.typeArrowImageView:
                typeSpinner.performClick();
                break;
            case R.id.governArrowImageView:
                governSpinner.performClick();
                break;
            case R.id.specializationArrowImageView:
                specializationSpinner.performClick();
                break;
        }
    }

    private void set(){
        typeArrowImageView.setOnClickListener(this);
        governArrowImageView.setOnClickListener(this);
        specializationArrowImageView.setOnClickListener(this);
    }

    private void decodeGovernResponse(){
        StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
        Gson gson = new Gson();
        standardWebServiceResponse = gson.fromJson(governResponse, StandardWebServiceResponse.class);
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        governModelArrayList = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getGoverns()), new TypeToken<List<GovernModel>>(){}.getType());
    }

    private void decodeSpecializationResponse(){
        StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
        Gson gson = new Gson();
        standardWebServiceResponse = gson.fromJson(specializationResponse, StandardWebServiceResponse.class);
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        specializationModelArrayList = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getSpecs()), new TypeToken<List<SpecializationModel>>(){}.getType());
    }

    private void spinnerAdapter(){
        ArrayAdapter<GovernModel> spinnerAdapter = new ArrayAdapter<GovernModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, governModelArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        governSpinner.setAdapter(spinnerAdapter);

        ArrayAdapter<SpecializationModel> spinnerAdapter2 = new ArrayAdapter<SpecializationModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, specializationModelArrayList);
        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        specializationSpinner.setAdapter(spinnerAdapter2);
    }
}