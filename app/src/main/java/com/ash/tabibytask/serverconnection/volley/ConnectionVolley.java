package com.ash.tabibytask.serverconnection.volley;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.ash.tabibytask.R;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shafeek on 21/09/16.
 */
public class ConnectionVolley extends StringRequest {

    private String url;
    Map<String, String> params;
    Map<String, String> headers;
    Context context;
    public static Dialog dialog;
    boolean dialogStatus;
    String requestBody;

    public ConnectionVolley(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, Map<String, String> params, boolean dialogStatus) {
        super(method, url, listener, errorListener);
        this.params = params;
        this.context = context;
        this.dialogStatus = dialogStatus;
        this.requestBody = "";
        this.headers = new HashMap<>();
        loadingDialog();
    }

    public ConnectionVolley(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, String requestBody, boolean dialogStatus) {
        super(method, url, listener, errorListener);
        this.params = new HashMap<>();
        this.context = context;
        this.dialogStatus = dialogStatus;
        this.requestBody = requestBody;
        this.headers = new HashMap<>();
        loadingDialog();
    }

    public ConnectionVolley(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, Map<String, String> params, Map<String, String> headers, boolean dialogStatus) {
        super(method, url, listener, errorListener);
        this.params = params;
        this.context = context;
        this.dialogStatus = dialogStatus;
        this.headers = headers;
        loadingDialog();
    }

    public ConnectionVolley(String url, Response.Listener<String> listener, Response.ErrorListener errorListener, Map<String, String> params) {
        super(url, listener, errorListener);
        this.params = params;
    }

    protected void loadingDialog() {
        if (dialogStatus) {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    @Override
    protected void deliverResponse(String response) {
        super.deliverResponse(response);
        try {
            dialog.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        return super.parseNetworkResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        headers = new HashMap<>();
        headers.put("content-type", "application/json");
        return headers;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return requestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
            return null;
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}
